package org.smartboot.socket.protocol;

/**
 * @author 三刀
 * @version V1.0 , 2017/8/30
 */
public enum  HttpPart {
    HEAD,BODY,END;
}
